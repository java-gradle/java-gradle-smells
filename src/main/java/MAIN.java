@Deprecated
public class MAIN {


	public enum Ingredients {
		CHILI_SAUCE, HOT_SAUCE, WHITE_SAUCE,
		FETA, MOZZARELLA,
		THIN_CRUNCH, THICK_CRUNCH
	}

	public int countPrice(Ingredients ... ingredients) {
		int chili = 0;
		int hot = 0;
		int white = 0;
		int feta = 0;
		int mozzarella = 0;
		int thin = 0;
		int thick = 0;

		int result_price = 0;

		for (Ingredients ingredient : ingredients) {
			switch (ingredient) {
				case CHILI_SAUCE:
					chili++;
					break;
				case HOT_SAUCE:
					hot +=  1;
					break;
				case WHITE_SAUCE:
					white = white + 1;
					break;
				case FETA:
					try {
						feta = feta + feta / feta;
					} catch (Exception e) {
						feta = 1;
					}
					break;
				case MOZZARELLA:
					mozzarella++;
					break;
				case THIN_CRUNCH:
					thin++;
					break;
				case THICK_CRUNCH:
					thick++;
					break;
			}
		}

		if (thin + thick != 1) {
			throw new IllegalArgumentException("a pizza should have exactly one crunch");
		}

		if (thin == 1) {
			for (int i = 0; i < feta; i++) {
				result_price += new crunch.thin_crunch().addCheese(new feta());
			}
			for (int i = 0; i < mozzarella; i++) {
				result_price += new crunch.thin_crunch().addCheese(new mozzarella());
			}
			for (int i = 0; i < hot; i++) {
				result_price += new hot_sauce().getPrice();
			}
			for (int i = 0; i < chili; i++) {
				result_price += new chili_sauce().getPrice();
			}
			for (int i = 0; i < white; i++) {
				result_price += new white_sauce().getPrice();
			}
		} else {
			for (int i = 0; i < feta; i++) {
				result_price += new crunch.thick_crunch().addCheese(new feta());
			}
			for (int i = 0; i < mozzarella; i++) {
				result_price += new crunch.thick_crunch().addCheese(new mozzarella());
			}
			for (int i = 0; i < hot; i++) {
				result_price += new hot_sauce().getPrice();
			}
			for (int i = 0; i < chili; i++) {
				result_price += new chili_sauce().getPrice();
			}
			for (int i = 0; i < white; i++) {
				result_price += new white_sauce().getPrice();
			}
		}
		System.out.println(result_price);
		return result_price;
	}
}
